package players;

import messages.OfferMsg;
import messages.RequestMsg;
import messages.TCPmsg;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.logging.Logger;

/**
 * Created by Shir-el on 05/01/2017.
 */
public class Client extends Player {
    private static final Logger LOGGER = Logger.getLogger(Client.class.getName());
    private final String[] stateName = {"looking", "connected", "done"}; //0-looking 1-connected 2-done
    private byte[] remoteServerIP;
    private short remoteServerPort;
    private boolean isFoundServer;
    private Socket clientTCPSocket;
    private DatagramSocket clientUDPSocket;
    private OfferMsg offerMsg;

    public Client(int state) throws Exception {
        super(state);
        isFoundServer = false;

        remoteServerIP = new byte[4];
        remoteServerPort = 0;

        try {
            this.clientUDPSocket = new DatagramSocket();
        } catch (SocketException e) {
            LOGGER.info("Client was not able to open udp socket");
            throw new Exception();
        }
    }

    private int changeStateNameToStateNum(String nameOfState) {
        switch (nameOfState) {
            case "looking":
                return 0;
            case "connected":
                return 1;
            case "done":
                return 2;
        }
        return 0;
    }

    public String[] getStateName() {
        return stateName;
    }

    @Override
    public String toString() {
        return "Client{" +
                "state=" + getState() +
                '}';
    }

    public boolean lookForServer_sendReqMsg(RequestMsg requestMsg) {
        if (isFoundServer) {
            LOGGER.info("Already connected to remote server");
            return true;
        }

        DatagramPacket sendPacket = null;
        try {
            sendPacket = new DatagramPacket(requestMsg.getBytes(), requestMsg.getBytes().length, InetAddress.getByName("255.255.255.255"), 6000);
        } catch (UnknownHostException e) {
            LOGGER.info("Error creating UDP packet");
            return false;
        }
        try {
            clientUDPSocket.send(sendPacket);
            LOGGER.info("Sent request message.");
        } catch (IOException e) {
            LOGGER.info("Error trying to send request msg");
            return false;
        }

        try {
            clientUDPSocket.setSoTimeout(1000);
        } catch (SocketException e) {
            LOGGER.info("Error trying to set timeout to the UDP client socket");
        }


        byte[] receiveOfferMsg = new byte[26];
        long t = System.currentTimeMillis();
        long end = System.currentTimeMillis() + 1000;
        while (t < end) {
            DatagramPacket receiveOfferMsgPacket = new DatagramPacket(receiveOfferMsg, receiveOfferMsg.length);
            try {
                LOGGER.info("Trying to receive offer message");
                clientUDPSocket.receive(receiveOfferMsgPacket);
            } catch (IOException e) {
                LOGGER.info("Error to receive offer msg");
                return false;
            }

            OfferMsg offerMsg = arrayByteToOfferMsg(receiveOfferMsgPacket.getData());
            if (requestMsg.getNumber() == offerMsg.getNumber()) {
                LOGGER.info("Received offer message from "+offerMsg.getIdTransmitter());
                this.offerMsg= offerMsg;
                this.remoteServerIP[0] = offerMsg.getIpAddress()[0];
                this.remoteServerIP[1] = offerMsg.getIpAddress()[1];
                this.remoteServerIP[2] = offerMsg.getIpAddress()[2];
                this.remoteServerIP[3] = offerMsg.getIpAddress()[3];
                this.remoteServerPort = offerMsg.getPort();
                this.isFoundServer = true;
                try {
                    this.clientTCPSocket = new Socket(InetAddress.getByAddress(this.remoteServerIP), this.remoteServerPort);
                    LOGGER.info("Client is now connected to remote server "+offerMsg.getIdTransmitter());
                } catch (IOException e) {
                    LOGGER.info("Error in client trying to connect to remote server");
                    return false;
                }
                return true;
            } else
                return false;
        }
        return false;
    }

    public boolean sendMessage(TCPmsg gameMsg) throws Exception {
        try {
            DataOutputStream outToServer = new DataOutputStream(clientTCPSocket.getOutputStream());
            outToServer.write(gameMsg.getData().getBytes());
            LOGGER.info("Sent message to server "+ offerMsg.getIdTransmitter());
            return true;
        } catch (IOException e) {
            LOGGER.info("Error while trying to send msg to the remote server.");
            throw new Exception("Error while trying to send msg to the remote server.");
        }
    }

    public boolean done() {
        setState(2);//done
        return true;
    }

    public OfferMsg arrayByteToOfferMsg(byte[] str) {
        if (str.length != 26)
            return null;
        String idTransCheck = new String(str, 0, 16);
        if (idTransCheck.contains("Networking17") == false)
            return null;

        byte[] intInBytes = new byte[4];
        intInBytes[0] = str[16];
        intInBytes[1] = str[17];
        intInBytes[2] = str[18];
        intInBytes[3] = str[19];
        int reqNumber = ByteBuffer.wrap(intInBytes).getInt();

        byte[] ipOfOffer = new byte[4];
        ipOfOffer[0] = str[20];
        ipOfOffer[1] = str[21];
        ipOfOffer[2] = str[22];
        ipOfOffer[3] = str[23];

        byte[] shortInBytes = new byte[2];
        shortInBytes[0] = str[24];
        shortInBytes[1] = str[25];
        short port = ByteBuffer.wrap(shortInBytes).getShort();

        return new OfferMsg(idTransCheck, reqNumber, ipOfOffer, port);
    }

    public void closeAllSockets() {
        if (clientTCPSocket != null) {
            try {
                LOGGER.info("Closing all sockets");
                clientTCPSocket.close();
            } catch (IOException e) {
            }
        }
        clientUDPSocket.close();
    }

}
