package players;

import java.nio.ByteBuffer;

/**
 * Created by Shir-el on 05/01/2017.
 */
public abstract class Player {
    private int state;

    public Player(int state) {
        this.state = state;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
}
