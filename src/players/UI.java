package players;

import messages.Message;

import java.util.Scanner;

/**
 * Created by Shir-el on 05/01/2017.
 */
public class UI extends Player {
    private final String[] stateName = {"off", "on"}; //0-off 1-on

    public UI(int state) {
        super(state);
    }

    public String[] getStateName() {
        return stateName;
    }

    private int changeStateNameToStateNum(String nameOfState) {
        switch (nameOfState) {
            case "off":
                return 0;
            case "on":
                return 1;
        }
        return 0;
    }

    @Override
    public String toString() {
        return "UI{" +
                "state=" + getState() +
                '}';
    }

    public String getMessage() {
        //get message from user
        if (stateName[getState()].equals("off")) {
            Scanner in = new Scanner(System.in);
            System.out.println("Enter a string");
            String msg=in.nextLine();
            return (msg+"\n");
        }
        return null;
    }

    public boolean printMessage(Message msg) {
        //print message to user's screen
        if (stateName[getState()].equals("on")) {
            System.out.println(msg.getData());
            return true;
        }
        else{
            return false;
        }
    }
}
