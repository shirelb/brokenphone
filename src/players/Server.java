package players;

import messages.OfferMsg;
import messages.RequestMsg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.logging.Logger;

/**
 * Created by Shir-el on 05/01/2017.
 */
public class Server extends Player {
    private static final Logger LOGGER = Logger.getLogger( Server.class.getName() );
    private ServerSocket serverTCPSocket;
    private Socket connectionTCPSocket;
    private DatagramSocket serverUDPSocket;
    private short serverTCPport;
    private final String[] stateName = {"looking", "connected"}; //0-looking 1-connected
    private RequestMsg requestMsg;
    private DatagramPacket receivePacketRequestMsg;
    private String TCPmsg;
    private boolean isConnectedToRemoteClient;

    public Server(int state) {
        super(state);
        TCPmsg = "";
        this.serverTCPport = 0;

        this.connectionTCPSocket = null;
        this.isConnectedToRemoteClient=false;
        try {
            this.serverUDPSocket = new DatagramSocket(6000);
        } catch (SocketException e) {
        }
    }

    public String getTCPmsg() {
        return TCPmsg;
    }

    private int changeStateNameToStateNum(String nameOfState) {
        switch (nameOfState) {
            case "looking":
                return 0;
            case "connected":
                return 1;
        }
        return 0;
    }

    public String[] getStateName() {
        return stateName;
    }

    @Override
    public String toString() {
        return "Server{" +
                "state=" + getState() +
                '}';
    }

    private boolean findTCPportAndListenOnIt() {
        short portNumber = 6001;
        while (portNumber <= 7000) {
            try {
                this.serverTCPSocket = new ServerSocket(portNumber);
                this.serverTCPport = portNumber;
                LOGGER.info("Created TCP socket on port " + this.serverTCPport);
                try {
                    this.serverTCPSocket.setSoTimeout(1000);
                } catch (SocketException e) {
                    LOGGER.info("Error trying to set timeout for server TCP socket");
                }
                return true;
            } catch (IOException e1) {
                LOGGER.info("Error trying to find port and listen on it in TCP socket");
                try {
                    this.serverTCPSocket.close();
                } catch (IOException e) {
                    LOGGER.info("Error while closing socket");
                }
                this.serverTCPport = 0;
                portNumber++;
            }
        }
        return false;
    }

    public boolean listenToTCPsocket() throws Exception {
        boolean result = false;
        if (this.connectionTCPSocket != null){
                BufferedReader inFromClient = null;
                try {
                    inFromClient = new BufferedReader(new InputStreamReader(connectionTCPSocket.getInputStream()));
                } catch (IOException e) {
                    LOGGER.info("Error while opening a new buffer reader");
                    return false;
                }
                try {
                    this.TCPmsg = inFromClient.readLine();
                    if(this.TCPmsg == null)
                    {
                        LOGGER.info("Error trying to read from client in TCP socket");
                        throw new Exception();
                    }
                    LOGGER.info("Got message from client " + this.requestMsg.getIdTransmitter()+" : " + this.TCPmsg);
                    this.TCPmsg = this.TCPmsg + "\n";
                } catch (IOException e) {
                    LOGGER.info("Error trying to read from client in TCP socket");
                    throw new Exception();
                }

            if (this.TCPmsg.length() > 0)
                result = true;
                return result;
        }
        else {
            return result;
        }
    }

    public short getServerTCPport() {
        return serverTCPport;
    }

    public boolean listenToUDPsocket() {
        if(isConnectedToRemoteClient)
            return true;

        byte[] receiveData = new byte[20];
        try {
            serverUDPSocket.setSoTimeout(1000);
        } catch (SocketException e) {
            LOGGER.info("Error trying to set timeout for server UDP socket");
        }
        long t = System.currentTimeMillis();
        long end = System.currentTimeMillis() + 1000;
        while (t < end) {
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            try {
                LOGGER.info("Trying to receive request message");
                serverUDPSocket.receive(receivePacket);
            } catch (IOException e) {
                LOGGER.info("Error receiving request message");
                return false;
            }
            String str = new String(receivePacket.getData());
            try {
                LOGGER.info("Recived request message");
                if (NetworkInterface.getByInetAddress(receivePacket.getAddress()) == null) {
                    if(this.serverTCPSocket == null || !isIPAddressEqual(receivePacket.getAddress().getAddress(),this.serverTCPSocket.getInetAddress().getAddress())) {
                        if (str.contains("Networking17") && str.length() == 20) {
                            this.requestMsg = arrayByteToRequestMsg(receivePacket.getData());
                            LOGGER.info("Received in packet: " + this.requestMsg.getData());
                            this.receivePacketRequestMsg = receivePacket;
                            return true;
                        } else
                            return false;
                    }
                }
            } catch (SocketException e) {
                return false;
            }
        }
        return false;
    }

    private boolean isIPAddressEqual(byte[] IPaddress1, byte[] IPaddress2) {
        for (int i = 0; i < IPaddress1.length; i++) {
            if (IPaddress1[i] != IPaddress2[i])
                return false;
        }
        return true;
    }

    public RequestMsg arrayByteToRequestMsg(byte[] str) {
        if (str.length != 20)
            return null;
        String idTransCheck=new String(str,0,16);
        if (idTransCheck.contains("Networking17") == false)
            return null;

        byte[] intInBytes=new byte[4];
        intInBytes[0]=str[16];
        intInBytes[1]=str[17];
        intInBytes[2]=str[18];
        intInBytes[3]=str[19];
        int reqNumber= ByteBuffer.wrap(intInBytes).getInt();

        return new RequestMsg(idTransCheck, reqNumber);
    }

    public boolean sendOfferMsgAndWaitForConnection(String AppName) {
        if(isConnectedToRemoteClient)
            return true;


        if (this.serverTCPport == 0) {
            if (findTCPportAndListenOnIt() == false)
                return false;
        }

        //send true if the connection is created (after the client connect to server. else false.
        OfferMsg offerMsg = null;
        try {
            offerMsg = new OfferMsg(AppName, requestMsg.getNumber(), Inet4Address.getLocalHost().getAddress(), this.serverTCPport);
        } catch (UnknownHostException e) {
            LOGGER.info("Error creating offer message");
            return false;
        }

        InetAddress IPAddress = receivePacketRequestMsg.getAddress();
        int port = receivePacketRequestMsg.getPort();
        DatagramPacket sendOfferMsgPacket = new DatagramPacket(offerMsg.getBytes(), offerMsg.getBytes().length, IPAddress, port);
        try {
            LOGGER.info("Sending offer message to ");
            serverUDPSocket.send(sendOfferMsgPacket);
        } catch (IOException e) {
            LOGGER.info("Error sending offer message");
            return false;
        }
        return this.tryToAcceptConnect();
    }

    public void closeAllSockets() {
        LOGGER.info("Closing all sockets");
        if (serverTCPSocket != null){
            try {
                serverTCPSocket.close();
            } catch (IOException e) {
                LOGGER.info("Error closing socket serverTCPSocket");
            }
        }
        if (connectionTCPSocket != null){
            try {
                connectionTCPSocket.close();
            } catch (IOException e) {
                LOGGER.info("Error closing socket connectionTCPSocket");
            }
        }
        serverUDPSocket.close();
    }

    private boolean tryToAcceptConnect() {
        boolean result = false;

        if (this.serverTCPport > 6000 && this.serverTCPport <= 7000) {
            try {
                LOGGER.info("------ Waiting to connection -------");
                this.connectionTCPSocket = this.serverTCPSocket.accept();
                isConnectedToRemoteClient=true;
                result = true;
                LOGGER.info("We are connected to a client"+ this.requestMsg.getIdTransmitter());
            } catch (IOException e) {
                e.printStackTrace();
                this.serverTCPport = 0;
                try {
                    this.serverTCPSocket.close();
                } catch (IOException e1) {
                    LOGGER.info("Error closing socket");
                   return result;
                }
            }
        }
        return result;
    }
}
