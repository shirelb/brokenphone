package messages;

/**
 * Created by Shir-el on 04/01/2017.
 */
public class TCPmsg extends Message {

    public TCPmsg(String data){
        super(data);
    }

    @Override
    public String toString() {
        return "TCPmsg{"+"data: "+ super.getData()+"}";
    }
}
