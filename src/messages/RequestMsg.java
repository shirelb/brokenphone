package messages;

import java.nio.ByteBuffer;

/**
 * Created by Shir-el on 04/01/2017.
 */
public class RequestMsg extends Message {
    private String idTransmitter; // need to be  bytes
    private int number;
    private byte[] requestMsgBytes;

    public RequestMsg(String idTrans){
        super("RequestMsg");
        this.idTransmitter = idTrans;
        this.number = (int) Math.round(Math.random()*(2^32));

        this.requestMsgBytes= new byte[20];

        putBytesInRequestMsgBytes();
        setData(this.toString());
    }

    @Override
    public byte[] getBytes() {
        return requestMsgBytes;
    }

    private void putBytesInRequestMsgBytes(){
        byte[] idTransBytes=idTransmitter.getBytes();
        byte[] numberBytes= ByteBuffer.allocate(4).putInt(number).array();

        copyBytesToRequestMsgBytes(idTransBytes,0);
        copyBytesToRequestMsgBytes(numberBytes,16);
    }

    private void copyBytesToRequestMsgBytes(byte[] from,int indexFrom)
    {
        for(int i=0;i<from.length;i++)
            requestMsgBytes[indexFrom+i]=from[i];
    }

    public RequestMsg(String idTransmitter, int number) {
        super("RequestMsg");
        this.idTransmitter = idTransmitter;
        this.number = number;
        setData(this.toString());
    }

    public String getIdTransmitter() {
        return idTransmitter;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return "RequestMsg{" +
                "idTransmitter='" + idTransmitter + '\'' +
                ", number=" + number +
                '}';
    }

    @Override
    public int msgLength() {
        return requestMsgBytes.length;
    }
}
