package messages;

import java.nio.ByteBuffer;

/**
 * Created by Shir-el on 04/01/2017.
 */
public class OfferMsg extends Message {
    private String idTransmitter; //need to be 16 bytes
    private int number;
    private byte[] ipAddress;
    private short port;
    private byte[] offerMsgBytes;


    public OfferMsg(String idTrans ,int num ,byte[] ip,short portNumber){
        super("OfferMsg");
        this.idTransmitter = idTrans;
        this.number = num;

        this.ipAddress= new byte[4];
        this.ipAddress[0] = ip[0];
        this.ipAddress[1] = ip[1];
        this.ipAddress[2] = ip[2];
        this.ipAddress[3] = ip[3];

        this.port = portNumber;

        offerMsgBytes= new byte[26];
        putBytesInOfferMsgBytes();

        setData(this.toString());
    }

    private void putBytesInOfferMsgBytes(){
        byte[] idTransBytes=idTransmitter.getBytes();
        byte[] numberBytes= ByteBuffer.allocate(4).putInt(number).array();
        byte[] portBytes=ByteBuffer.allocate(2).putShort(port).array();

        copyBytesToOfferMsgBytes(idTransBytes,0);
        copyBytesToOfferMsgBytes(numberBytes,16);
        copyBytesToOfferMsgBytes(ipAddress,20);
        copyBytesToOfferMsgBytes(portBytes,24);
    }

    private void copyBytesToOfferMsgBytes(byte[] from,int indexFrom)
    {
        for(int i=0;i<from.length;i++)
            offerMsgBytes[indexFrom+i]=from[i];
    }

    @Override
    public byte[] getBytes() {
        return offerMsgBytes;
    }

    @Override
    public String toString() {
        return "OfferMsg{" +
                "idTransmitter='" + idTransmitter + '\'' +
                ", number=" + number +
                ", ipAddress=" + ipAddress[0]+"."+ ipAddress[1]+"."+ipAddress[2]+"."+ipAddress[3] +
                ", port=" + port +
                '}';
    }

    @Override
    public int msgLength() {
        return offerMsgBytes.length;
    }

    public String getIdTransmitter() {
        return idTransmitter;
    }

    public int getNumber() {
        return number;
    }

    public byte[] getIpAddress() {
        return ipAddress;
    }

    public short getPort() {
        return port;
    }
}
