package messages;

import java.nio.ByteBuffer;

/**
 * Created by Shir-el on 04/01/2017.
 */
public class Message {
    private String data;

    public Message() {
        data = "";
    }

    public Message(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public int msgLength(){
        return data.length();
    }

    public byte[] getBytes(){
        return data.getBytes();
    }

    public void setData(String data) {
        this.data = data;
    }

    public String intToStringIn4Bytes(int numebr){
        String ans="";
        byte[] intInBytes=ByteBuffer.allocate(4).putInt(numebr).array();
        for(int i=0;i<intInBytes.length;i++)
            ans=ans+(char)intInBytes[i];
        return ans;
    }

    public String shortToStringIn2Bytes(short numebr){
        String ans="";
        byte[] intInBytes=ByteBuffer.allocate(2).putShort(numebr).array();
        for(int i=0;i<intInBytes.length;i++)
            ans=ans+(char)intInBytes[i];
        return ans;
    }
}
