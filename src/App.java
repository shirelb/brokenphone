import messages.RequestMsg;
import messages.TCPmsg;
import players.Client;
import players.Server;
import players.UI;

import java.util.logging.Logger;


/**
 * Created by Shir-el on 02/01/2017.
 */
public class App {
    private static final Logger LOGGER = Logger.getLogger( App.class.getName() );
    private Server server;
    private Client client;
    private UI ui;
    private String AppName= "Networking17_lll";

    private int mode;
    private String[] modeName;

    public App() throws Exception {
        this.server = new Server(0); //initial state: looking
        this.client = new Client(0); //initial state: looking
        this.ui = new UI(0); //initial state: off

        this.mode = 0;
        this.modeName = new String[4];
        this.modeName[0] = "rx_off_tx_off";
        this.modeName[1] = "rx_off_tx_on";
        this.modeName[2] = "rx_on_tx_off";
        this.modeName[3] = "rx_on_tx_on";
    }

    public int rx_off_tx_off() {
        LOGGER.info("Entering rx off tx off mode");

        RequestMsg requestMsg = new RequestMsg(AppName);

        while (true) {
            if (server.listenToUDPsocket()) { //true- if received request msg
                if (server.sendOfferMsgAndWaitForConnection(AppName)){
                    server.setState(1); // set server mode to connected
                    client.setState(2);
                    ui.setState(1); //set state to done
                    this.mode = 2;
                    return this.mode;
                }
            }
            if (client.lookForServer_sendReqMsg(requestMsg)) //true if offer msg received
            {
                client.setState(1); //set client to state connected
                this.mode = 1; //rx_off_tx_on
                return this.mode;
            }

        }
    }

    public int rx_off_tx_on() throws Exception {
        LOGGER.info("Entering rx off tx on mode");
        String ui_string = "";
        while (true) {
            ui_string = ui.getMessage();

            client.sendMessage(new TCPmsg(ui_string));
            if (server.listenToUDPsocket()) {
                if (server.sendOfferMsgAndWaitForConnection(AppName)){
                    server.setState(1); // set server mode to connected
                    ui.setState(0);
                    this.mode = 3;
                    return this.mode;
                }
            }
        }
    }

    public int rx_on_tx_off() throws Exception {
        LOGGER.info("Entering rx on tx off mode");
        while (true) {
            if (server.listenToTCPsocket())
                ui.printMessage(new TCPmsg(server.getTCPmsg()));
        }
    }

    public int rx_on_tx_on() throws Exception {
        LOGGER.info("Entering rx on tx on mode");
        String msg = "";
        while (true) {
            if (server.listenToTCPsocket()) {
                msg = changeChar(server.getTCPmsg());
                client.sendMessage(new TCPmsg(msg));
            }
        }
    }

    public void play() {
        LOGGER.info( "my name is: "+AppName);
        try{
            if (rx_off_tx_off() == 1) {
                if (rx_off_tx_on() == 3) {
                    rx_on_tx_on();
                }
            } else {
                rx_on_tx_off();
            }
        }
        catch (Exception e){
            LOGGER.info( "System ran into an error while operating");
            server.closeAllSockets();
            client.closeAllSockets();
            System.exit(1);
        }

    }

    public static void main(String[] args) {
        App app = null;

        try {
            app = new App();
        } catch (Exception e) {
            LOGGER.info( "Error while init");
            System.exit(1);
        }

        app.play();
    }

    private static String changeChar(String before_str) {
        String after_str = new String(before_str);
        char[] strInChars = new char[before_str.length()];
        int i = (int) (Math.random() * (after_str.length() - 2)); //choose the index in the string that will change that is not the \n
        strInChars = before_str.toCharArray();
        char charBefor =  strInChars[i];
        char charAfter = (char) ((int) (Math.random() * (93)) + 33); //choose the new char that will replace the old one
        while(charBefor == charAfter)
            charAfter = (char) ((int) (Math.random() * (93)) + 33); //choose the new char that will replace the old one
        strInChars[i] = charAfter;
        after_str = new String(strInChars);
        return after_str;
    }
}